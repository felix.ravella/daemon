<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Connexion</title>
</head>
<body>
<?php 
  session_start();
?>

<div id="top-banner">
<?php
  if (isset($_SESSION['user'])) {
    echo "<div>Energie: " . $_SESSION['user']['stamina'] . "</div>";
  }
?>

  <form  action="../controllers/auth.php" method="POST">
    <input name="mode" hidden="true" value="logout">
    <button type="submit" name="submit"> Se deconnecter </button>
  </form>

</div>
