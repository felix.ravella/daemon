<?php
session_start();
require_once '../functions.php';

// TODO hasher MdP
function connect($login,$password){
  $bdd = getPDO();
  $query = $bdd->prepare("SELECT * FROM user WHERE login=:login AND password=:password");
  $query->bindParam(":login", $login);
  $query->bindParam(":password", $password);
  $query->execute();
  
  $matches = $query->fetchAll();
  if (!empty($matches)) {
    $_SESSION['user'] = $matches[0];
    return true;
  } else {
    return false;
  }
}

?>
