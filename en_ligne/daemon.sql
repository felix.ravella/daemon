-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 13 avr. 2022 à 12:15
-- Version du serveur :  8.0.27-0ubuntu0.21.04.1
-- Version de PHP : 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `daemon`
--

-- --------------------------------------------------------

--
-- Structure de la table `daemon`
--

CREATE TABLE `daemon` (
  `id` int NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'EntrezUnNom',
  `growth` int NOT NULL DEFAULT '0',
  `strength` int NOT NULL DEFAULT '0',
  `intelligence` int NOT NULL DEFAULT '0',
  `charm` int NOT NULL DEFAULT '0',
  `stamina` int NOT NULL DEFAULT '0',
  `fight` int NOT NULL DEFAULT '0',
  `magic` int NOT NULL DEFAULT '0',
  `knowledge` int NOT NULL DEFAULT '0',
  `arts` int NOT NULL DEFAULT '0',
  `style` int NOT NULL DEFAULT '0',
  `userId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `login` varchar(25) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `stamina` int DEFAULT NULL,
  `money` int DEFAULT NULL,
  `lastConnectionAt` timestamp NULL DEFAULT NULL,
  `nextStaminaAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `daemon`
--
ALTER TABLE `daemon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_userId` (`userId`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `daemon`
--
ALTER TABLE `daemon`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `daemon`
--
ALTER TABLE `daemon`
  ADD CONSTRAINT `fk_user_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
