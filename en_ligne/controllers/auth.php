<?php
session_start();

require_once '../models/user.php';
require_once '../models/auth.php';

if (isset($_POST['submit']) && isset($_POST['mode'])) {
  echo "subscribe controller<br>";
  switch ($_POST['mode']) {
    case 'login':
      login();
      break;
    case 'subscribe':
      subscribe();
      break;
    case 'logout':
      logout();
      break;
    default:
      header("Location: ../vues/index.php");
  }
} else {
  header("Location: ../vues/index.php");
}

function login() {
  try {
    $login = $_POST['login'];
    $password = $_POST['password'];

    if (!$login || !$password) {
      throw new Exception("Certains champs ne sont pas renseignés.");
    }
    else {
      if (connect($login, $password)) {
        header('Location: ../vues/daemon.php');
      } else {
        header('Location: ../vues/index.php');
      }
    }
  } catch (Exception $e) {
    print_r($e);
  }
  
}

function subscribe() {
  try {
    $login = $_POST['login'];
    $password = $_POST['password'];
    $passwordConfirm = $_POST['passwordConfirm'];

    if (!$login || !$password || !$passwordConfirm) {
      throw new Exception ("Certains champs ne sont pas renseignés.");
    }
    else if ($password != $passwordConfirm) {
      throw new Exception ("Le mot de passe ne correspond pas à sa confirmation.");
    }
    else {
      if (userAlreadyExists($login)){
        throw new Exception ("Cet identifiant n'est pas disponible.");
      }
      else {
        echo "b<br>";
        createUser($login, $password);
        echo "c<br>";
      }
    }
  } catch (Exception $e) {
    print_r($e);
    header('Location: ../vues/subscribe.php');
  }
}

function logout() {
  $_SESSION['user'] = [];
  $_SESSION['daemon'] = [];
  session_destroy();
  header('Location: ../vues/index.php');
}

?>
