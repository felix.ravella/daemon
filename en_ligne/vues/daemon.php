<?php 
session_start();
require_once '../includes/header.php';
require_once '../controllers/daemon.php';
?>


<h2>Mon Daemon</h2>

<?php if ($daemon != []): ?>
  <h3><?= $daemon['name'] ?></h3>
  <img src="../assets/poring.gif">
  <div id="stats">
    
    <b>Force: </b> <?= $daemon['strength'] ?><br>
    <b>Intelligence: </b> <?= $daemon['intelligence'] ?><br>
    <b>Charme: </b> <?= $daemon['charm'] ?><br>
  </div>
    
  <br>
  <div id="skills">
    <b>Combat: </b> <?= $daemon['fight'] ?><br>
    <b>Endurance: </b> <?= $daemon['stamina'] ?><br>
    <b>Magie: </b> <?= $daemon['magic'] ?><br>
    <b>Savoir: </b> <?= $daemon['knowledge'] ?><br>
    <b>Arts: </b> <?= $daemon['arts'] ?><br>
    <b>Style: </b> <?= $daemon['style'] ?><br>
  
  </div>
<?php else: ?>
  <a href="../vues/createDaemon.php">Créer un daemon</a>
<?php endif; ?>


<?php require_once '../includes/footer.php'; ?>
