<?php 
  require_once '../includes/header.php';
?>

<h2>Inscription</h2>
<form action="../controllers/auth.php" method="POST">
  <input name="mode" hidden="true" value="subscribe">

  <label for="login">Login</label>
  <br>
  <input type="text" name="login">
  <br>
  <label for="password">Mot de Passe</label>
  <br>
  <input type="password" name="password">
  <br>
  <label for="passwordConfirm">Confrmer Mot de Passe</label>
  <br>
  <input type="password" name="passwordConfirm">
  <br>

  <button type="submit" name="submit">Envoyer</button>
</form>

<?php 
  require_once '../includes/footer.php';
?>
