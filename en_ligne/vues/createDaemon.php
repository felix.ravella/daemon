<?php 
session_start();
require_once '../includes/header.php';
require_once '../controllers/daemon.php';
?>

<h2>Creation Daemon</h2>

<form name="statsForm" action="../controllers/daemon.php" method="POST">
  <label for="name">Nom</label>
  <input type="text" name="name" oninput="validateForm()">
</form>

  <br>

Points restants:<br><div id="points">5</div>

Force:
<div class="stat-block">
  <button id="remove-strength-button" onclick="changeStat('strength',-1)" disabled>-</button>
  <div id="display-strength"> 0 </div>
  <button id="add-strength-button" onclick="changeStat('strength',1)">+</button>
</div>
Intelligence:
<div class="stat-block">
  <button id="remove-intelligence-button" onclick="changeStat('intelligence',-1)" disabled>-</button>
  <div id="display-intelligence"> 0 </div>
  <button id="add-intelligence-button" onclick="changeStat('intelligence',1)">+</button>
</div>
Charm:
<div class="stat-block">
  <button id="remove-charm-button" onclick="changeStat('charm',-1)" disabled>-</button>
  <div id="display-charm"> 0 </div>
  <button id="add-charm-button" onclick="changeStat('charm',1)">+</button>
</div>
<br>

<button id="createButton" disabled="true" onclick="create()"> Créer </button>


<?php require_once '../includes/footer.php'; ?>

<script type="text/javascript" src="../scripts/createDaemonForm.js"></script>

