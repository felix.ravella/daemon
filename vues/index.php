<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Connexion</title>
</head>
<body>


<h2>Connexion</h2>
<form action="../controllers/auth.php" method="POST">
  <input name="mode" hidden="true" value="login">

  <label for="login">Login</label><br>
  <input type="text" name="login"><br>
  <label for="password">Mot de Passe</label><br>
  <input type="password" name="password"><br>

  <button type="submit" name="submit">Se connecter</button>
</form>
<div>Pas encore de incrit? <a href="../vues/subscribe.php">Créer un compte</a></div>

<?php 
  require_once '../includes/footer.php';
?>
