var points = 5;
var stats = {
  "strength": 0,
  "intelligence": 0,
  "charm": 0
};

var removeButtons = {
  "strength": document.getElementById("remove-strength-button"),
  "intelligence": document.getElementById("remove-intelligence-button"),
  "charm": document.getElementById("remove-charm-button"),
}
var addButtons = [
  document.getElementById("add-strength-button"),
  document.getElementById("add-intelligence-button"),
  document.getElementById("add-charm-button")
]

var elementPoints = document.getElementById("points");

var elementCreateButton = document.getElementById("createButton");

function changeStat(statName, value) {
  points -= value;
  elementPoints.innerText = points;
  let newValue = stats[statName] + value;
  stats[statName] = newValue;

  document.getElementById(`display-${statName}`).innerText = newValue;

  removeButtons[statName].disabled = (newValue === 0) ? true : false;

  addButtons.forEach(element => {
    element.disabled = (points === 0) ? true: false;
  })
  
  console.log("azea");
  validateForm();
}

function validateForm() {
  elementCreateButton.disabled = (!!document.statsForm.name.value && points === 0) ? false : true;
}

function create() {
  if (!!document.statsForm.name.value && points === 0) {
    var statsJson = JSON.stringify(stats);

    var statsField = document.createElement("input");
    statsField.setAttribute("type", "hidden");
    statsField.setAttribute("name", "stats");
    statsField.setAttribute("value", statsJson);
    document.statsForm.appendChild(statsField);

    var modeField = document.createElement("input");
    modeField.setAttribute("type", "hidden");
    modeField.setAttribute("name", "mode");
    modeField.setAttribute("value", "create");
    document.statsForm.appendChild(modeField);

    document.statsForm.submit();

  } else {
    validateForm();
    alert("Veuillez entrer un nom et repartir tous les points.");
  }
}
