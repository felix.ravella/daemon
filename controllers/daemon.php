<?php
session_start();

require_once '../models/daemon.php';

$daemon = [];
if (isset($_SESSION['user'])){
  $daemon = getDaemonByUserId($_SESSION['user']['id']);
} else {
  header('Location: ../vues/index.php');
}

if (isset($_POST['mode'])) {
  switch ($_POST['mode']) {
    case 'create':
      create();
      break;
    default:
      header("Location: ../vues/daemon.php");
  }
}

function create(){
  try {
    $name = $_POST['name'];
    $stats = json_decode($_POST["stats"]);
    createDeamon($name, $stats->strength, $stats->intelligence, $stats->charm, $_SESSION["user"]["id"]);
    header('Location: ../vues/daemon.php');
  } catch (Exception $e) {
    print_r($e);
  }
}


?>
